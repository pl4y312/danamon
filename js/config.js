$(window).load(function(){
	var maxHeight=0;
	$.each($('.sync-height-grid > div[class*="col-"]'), function() {
	    maxHeight = Math.max(maxHeight, $(this).height());
	})
	
	$('.sync-height-grid > div[class*="col-"]').height(maxHeight);
})

$(document).ready(function(){

	/** Slideshow **/
	
	$('#slideshow .item').css('display', 'none');
	$('#slideshow .item').first().fadeIn().addClass('active');
	var slideInt = setInterval( "slideshow()", 3000);

	$('#slideshow-nav').append(function(){
		var nav = '<ul>';

		for(i=0; i<$('#slideshow .item').size(); i++){
			nav += '<li data-id="' + i + '"></li>';
		}
		nav += '</ul>';

		return nav;
	});

	$('#slideshow-nav li').first().addClass('active');

	$('#slideshow-nav li').on('click', function(){

		clearInterval(slideInt);

		$('#slideshow-nav li.active').removeClass('active');
		$(this).addClass('active');

		var index = $(this).data('id');

		$('#slideshow .item.active').fadeOut().removeClass('active');
		$("#slideshow .item:eq(" + index + ")").fadeIn().addClass('active');

		slideInt = setInterval( "slideshow()", 3000);

	});

	/** Tabs Configuration **/

	var activeTab = $('.tab-headers .current').attr("id");
	$(".tab-panels > div").not(activeTab).css("display", "none");
	
	$(".tab-headers li").click(function(e) {
        e.preventDefault();
        $(this).addClass("current");
        $(this).siblings().removeClass("current");
        var tab = $(this).attr("id");
        $(".tab-panels > div").not(tab).css("display", "none");
        $(tab).fadeIn();
    });

    /** Accordion Configuration **/

	$('.accordion li').not('.open').find('.body').css("display", "none");
	
	$('.accordion .btn').click(function(){
		if(!$(this).closest('li').hasClass('open')){
			$('li.open .body').slideUp();
			$('li.open .btn').html('SHOW').removeClass('btn-arrow-up').addClass('btn-arrow-down');
			$('li.open').removeClass('open');
			
			$(this).closest('.header').siblings('.body').slideDown();
			$(this).closest('li').addClass('open');
			$(this).html('HIDE').removeClass('btn-arrow-down').addClass('btn-arrow-up');
		} else {
			$(this).closest('li').removeClass('open');
			$(this).closest('.header').siblings('.body').slideUp();
			$(this).html('SHOW').removeClass('btn-arrow-up').addClass('btn-arrow-down');
		}
	});

	$('#advanced-search').hide();

	$('.btn-change-search').click(function(){
		$('#advanced-search, #basic-search').toggle();
	});

	/** Modal Configuration **/
	$('.modal').after('<div id="bg-modal"></div>');
	$('#modal-close').click(function(){
		$('.modal').fadeOut();
		$('#bg-modal').fadeOut();
	})

	$('.round-border-bottom').each(function(){
		var that = $(this);
		that.width(that.closest('.submenu').width() + 60);
	});

	$('table tbody tr:nth-child(2n)').addClass('alt');

});

function modal(id, e){
	e.preventDefault();
	$('#bg-modal').fadeIn();
	$('#' + id + '.modal').fadeIn();
}

function slideshow(){
	var $active = $('#slideshow .item.active');
	var $next = $active.next('.item');

	$active.fadeOut().removeClass('active');
	$('#slideshow-nav li.active').removeClass('active');

	if($next.length > 0){
		$next.fadeIn().addClass('active');
	} else {
		$('#slideshow .item').first().fadeIn().addClass('active');
	}

	var activeIndex = $('#slideshow .item.active').index();
	$('#slideshow-nav li:eq(' + activeIndex + ')').addClass('active');

}